<?php

declare(strict_types=1);

namespace Grifix\Repeater;

final class Repeater
{
    private function __construct()
    {
    }

    public static function repeatUntilNotNull(callable $function, int $maxTimes = 10): mixed
    {
        $time = 0;
        do {
            $result = $function();
            if (null !== $result) {
                return $result;
            }
            $time++;
        } while ($maxTimes > $time);

        return null;
    }
}
