# Installation
`composer require grifix/repeater`

# Usage
This code tries to get value from external service 10 times or until external service will return value that is not null:
```php
   Repeater::repeatUntilNotNull(function () use ($externalService): ?string {
        $result = $externalService->getValue();
        if(null === $result){
            sleep(1);
        }
        return $result;
    }, 10);
```

