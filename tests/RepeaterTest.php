<?php

declare(strict_types=1);

namespace Grifix\Reflection\Tests;

use Grifix\Repeater\Repeater;
use PHPUnit\Framework\TestCase;

final class RepeaterTest extends TestCase
{
    public function testItRepeatsUntilReturnsFalse(): void
    {
        $counter = new \stdClass();
        $counter->value = 0;
        $function = function () use ($counter) {
            $counter->value++;
            /** @phpstan-ignore-next-line */
            if (3 === $counter->value) {
                return $counter->value;
            }

            return null;
        };
        self::assertEquals(3, Repeater::repeatUntilNotNull($function));
    }

    public function testItRepeatsUntilMaxTimes(): void
    {
        $counter = new \stdClass();
        $counter->value = 0;
        $function = function () use ($counter) {
            $counter->value++;
        };

        Repeater::repeatUntilNotNull($function, 5);
        self::assertEquals(5, $counter->value);
    }
}
